﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using TUGraz.VectoCommon.InputData;
using VECTOTrailer.Util;

namespace VECTOTrailer.ViewModel.Interfaces
{
	public interface IJobEditViewModel 
	{

		string JobFile { get; }

		IDeclarationTrailerInputDataProvider InputDataProvider { get; set; }

		bool DeclarationMode { get; }
		ObservableCollection<Component> Components { get; }
		ICommand EditComponent { get; }
		Component SelectedComponent { get; }
		IComponentViewModel CurrentComponent { get; }

		ICommand SaveJob { get; }
		ICommand SaveAsJob { get; }
		ICommand CloseJob { get; }
		ICommand ValidateInput { get; }
		ICommand ShowValidationErrors { get; }
		ICommand RemoveValidationErrors { get; }

	}
}
