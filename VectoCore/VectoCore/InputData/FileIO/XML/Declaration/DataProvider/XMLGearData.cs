﻿using System.Xml;
using System.Xml.Linq;
using TUGraz.IVT.VectoXML;
using TUGraz.VectoCommon.InputData;
using TUGraz.VectoCommon.Resources;
using TUGraz.VectoCommon.Utils;
using TUGraz.VectoCore.InputData.FileIO.XML.Common;
using TUGraz.VectoCore.InputData.FileIO.XML.Declaration.Interfaces;
using TUGraz.VectoCore.Utils;

namespace TUGraz.VectoCore.InputData.FileIO.XML.Declaration.DataProvider
{
	public abstract class XMLAbstractGearData : AbstractXMLType
	{
		protected TableData _lossmap;
		protected DataSource _dataSource;

		protected XMLAbstractGearData(XmlNode gearNode, string sourceFile) : base(gearNode)
		{
			SourceFile = sourceFile;
		}

		public virtual string SourceFile { get; }


		public virtual DataSource DataSource
		{
			get {
				return _dataSource ?? (_dataSource = new DataSource() {
					SourceFile = SourceFile,
					SourceType = DataSourceType.XMLEmbedded,
					SourceVersion = XMLHelper.GetVersionFromNamespaceUri(SchemaNamespace)
				});
			}
		}

		protected abstract XNamespace SchemaNamespace { get; }

		#region Implementation of ITransmissionInputData

		public virtual int Gear
		{
			get {
				return XmlConvert.ToUInt16(
					BaseNode.Attributes?.GetNamedItem(XMLNames.Gearbox_Gear_GearNumber_Attr).InnerText ?? "0");
			}
		}

		public virtual double Ratio
		{
			get { return GetString(XMLNames.Gearbox_Gear_Ratio).ToDouble(double.NaN); }
		}

		public virtual TableData LossMap
		{
			get {
				return _lossmap ?? (_lossmap = XMLHelper.ReadTableData(
							AttributeMappings.TransmissionLossmapMapping,
							GetNodes(new[] { XMLNames.Gearbox_Gear_TorqueLossMap, XMLNames.Gearbox_Gear_TorqueLossMap_Entry })));
			}
		}

		public virtual double Efficiency
		{
			get { return double.NaN; }
		}

		public virtual NewtonMeter MaxTorque
		{
			get { return GetNode(XMLNames.Gearbox_Gears_MaxTorque, required: false)?.InnerText.ToDouble().SI<NewtonMeter>(); }
		}

		public virtual PerSecond MaxInputSpeed
		{
			get { return GetNode(XMLNames.Gearbox_Gear_MaxSpeed, required: false)?.InnerText.ToDouble().RPMtoRad(); }
		}

		public virtual TableData ShiftPolygon
		{
			get { return null; }
		}

		#endregion
	}

	// ---------------------------------------------------------------------------------------

	public class XMLGearDataV10 : XMLAbstractGearData, IXMLGearData
	{
		public static readonly XNamespace NAMESPACE_URI = XMLDefinitions.DECLARATION_DEFINITIONS_NAMESPACE_URI_V10;

		public const string XSD_TYPE = "GearDeclarationType";

		public static readonly string QUALIFIED_XSD_TYPE = XMLHelper.CombineNamespace(NAMESPACE_URI.NamespaceName, XSD_TYPE);

		public XMLGearDataV10(XmlNode gearNode, string sourceFile) : base(gearNode, sourceFile) { }

		#region Overrides of XMLAbstractGearData

		protected override XNamespace SchemaNamespace
		{
			get { return NAMESPACE_URI; }
		}

		#endregion
	}

	// ---------------------------------------------------------------------------------------

	public class XMLGearDataV20 : XMLGearDataV10
	{
		public new static readonly XNamespace NAMESPACE_URI = XMLDefinitions.DECLARATION_DEFINITIONS_NAMESPACE_URI_V20;

		public new const string XSD_TYPE = "GearDeclarationType";

		public new static readonly string QUALIFIED_XSD_TYPE = XMLHelper.CombineNamespace(NAMESPACE_URI.NamespaceName, XSD_TYPE);


		public XMLGearDataV20(XmlNode gearNode, string sourceFile) : base(gearNode, sourceFile) { }

		protected override XNamespace SchemaNamespace
		{
			get { return NAMESPACE_URI; }
		}
	}

	// ---------------------------------------------------------------------------------------

	public class XMLPrimaryVehicleBusTransmissionDataV01 : XMLGearDataV10
	{
		public new static readonly XNamespace NAMESPACE_URI = XMLDefinitions.DECLARATION_PRIMARY_BUS_VEHICLE_URI_V01;

		public new const string XSD_TYPE = "TransmissionGearPIFType";

		public new static readonly string QUALIFIED_XSD_TYPE = XMLHelper.CombineNamespace(NAMESPACE_URI.NamespaceName, XSD_TYPE);

		public XMLPrimaryVehicleBusTransmissionDataV01(XmlNode gearNode, string sourceFile) 
			: base(gearNode, sourceFile) { }

		protected override XNamespace SchemaNamespace
		{
			get { return NAMESPACE_URI; }
		}
	}
}
