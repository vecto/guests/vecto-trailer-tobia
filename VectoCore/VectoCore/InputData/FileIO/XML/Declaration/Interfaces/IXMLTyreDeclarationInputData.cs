﻿using TUGraz.VectoCommon.InputData;

namespace TUGraz.VectoCore.InputData.FileIO.XML.Declaration.Interfaces
{
	public interface IXMLTyreDeclarationInputData : ITyreDeclarationInputData, IXMLResource { }
}
