﻿
Imports System.Reflection
Imports System.Runtime.InteropServices

' Below is the General Information about the Attributes
' controlling the Assembly. Change these attribute values to modify the information
' associated with the Assembly.

' Review the values of the Assembly Attributes

<Assembly: AssemblyTitle("VECTO")> 
<Assembly: AssemblyDescription("Vehicle Energy Consumption Calculation Tool")> 
<Assembly: AssemblyCompany("")> 
<Assembly: AssemblyProduct("VECTO")> 
<Assembly: AssemblyCopyright("© European Commission 2012 - 2017")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the Typelib if this project is exposed to COM
<Assembly: Guid("175c31e7-2d95-4afb-afec-c4d7719177db")> 

' Version information for an assembly consists of the following four values:
'
'      Major Release
'      Minor Release
'      Build Number
'      Revision
'
' You can specify all the values or use the defaults for Build and Revision Numbers
' by entering "*" in them:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.2.0.0")> 
<Assembly: AssemblyFileVersion("3.2.0.0")> 
