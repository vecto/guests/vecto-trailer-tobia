#Input and Output

Vecto uses data files for input and output of data. These are stored in different formats which are listed here.


**Input:**

- [JSON](#json)
- [Vecto CSV](#csv)
- [XML](#xml-job-file-declaration-mode)

**Output:**

- [JSON](#json)
- [Vecto CSV](#csv)
- [XML Declaration Report](#xml-declaration-report)
